from django.db import transaction
from django.shortcuts import render

from game.models import Bot, EarthUser, BotState
from .credentials import *

# Create your views here.

def index(request):
    context = {'title': 'Bot management'}
    return render(request, 'bots/index.html', context)





def example(request):
    context = {}
    earth_user = EarthUser.objects.get_or_create(jid='jnanar@agayon.be')
    b = Bot.objects.get_or_create(name='Bot 4')
    bs = BotState(earth_user=earth_user[0], bot=b[0], jid='test', contacted_bot=False, name='Plop')
    bs.save()

    return render(request, 'bots/index.html', context)
