from django.shortcuts import render

from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import login, logout
from django.shortcuts import render, redirect
from django.core import serializers



# Create your views here.

def index(request):
    context = {'session': request.session}
    return render(request, 'chatter/index.html', context)
