from django.conf.urls import url

# from django.contrib.auth.views import login
# from django.contrib.auth.views import logout
from django.contrib.auth.views import LoginView, LogoutView
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
]