from django.conf.urls import url

# from django.contrib.auth.views import login
# from django.contrib.auth.views import logout
from django.contrib.auth.views import LoginView, LogoutView
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^clue/$', views.clue, name='clue'),
    url(r'^clean/$', views.clean, name='clean'),
    url(r'^status/$', views.status, name='status'),
    url(r'^withdraw/$', views.withdraw, name='withdraw'),
    url(r'^final/$', views.final, name='final'),
    url(r'^withdraw/steven/$', views.steven, name='steven'),
    url(r'^withdraw/4d45c10cd25a/$', views.secret_4d45c10cd25a, name='secret_4d45c10cd25a'),
    url(r'^withdraw/c265c9f626bs/$', views.secret_c265c9f626bs, name='secret_c265c9f626bs'),
    url(r'^withdraw/bonus_65465454781/$', views.bonus, name='bonus'),
    url(r'^profile/delete/$', views.delete, name='delete'),
    url(r'^faq/$', views.faq, name='faq'),
    url(r'^faq/help_movim$', views.help_movim, name='help_movim'),
    url(r'^login/$', views.xmpp_authentification, name='login'),
    url(r'^logout/$', LogoutView.as_view(template_name='game/registration/logged_out.html'), name='logout')

]
