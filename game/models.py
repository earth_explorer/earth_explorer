from django.db import models
from django.contrib.auth.models import AbstractUser
from _datetime import datetime
from django.utils.timezone import now
from django.db.models import Manager as GeoManager
from django.contrib.gis.db import models as gismodels


# TODO: use nice symbols to add on the stuff.
# https://www.google.com/search?client=firefox-b-d&q=Leaflet+marker+style

class MapCoordinate(gismodels.Model):
    name = models.CharField(max_length=256)
    geom = gismodels.PointField()
    objects = GeoManager()
    earth_user = models.ForeignKey('EarthUser', on_delete=models.CASCADE, default='')
    category = models.CharField(max_length=256)
    latitude = models.FloatField(max_length=50, )
    longitude = models.FloatField(max_length=50)
    fullname = models.CharField(max_length=256, default="")

    def __unicode__(self):
        return self.category

    def __str__(self):
        return self.fullname or self.name


class Bot(models.Model):
    number = models.IntegerField()
    name = models.CharField(max_length=100, default='')
    jid = models.CharField(max_length=200, default='', null=True, blank=True)
    description = models.TextField(max_length=600, default='', null=True, blank=True)
    clue0 = models.CharField(max_length=200, default='', blank=True)
    clue1 = models.CharField(max_length=200, default='', blank=True)
    clue2 = models.CharField(max_length=200, default='', blank=True)
    clue3 = models.CharField(max_length=200, default='', blank=True)
    clue4 = models.CharField(max_length=200, default='', blank=True)
    trigger0 = models.CharField(max_length=50, default='', blank=True)
    trigger1 = models.CharField(max_length=50, default='', blank=True)
    trigger2 = models.CharField(max_length=50, default='', blank=True)
    trigger3 = models.CharField(max_length=50, default='', blank=True)
    trigger4 = models.CharField(max_length=50, default='', blank=True)

    def __str__(self):
        return self.jid


class BotState(models.Model):
    earth_user = models.ForeignKey('EarthUser', on_delete=models.CASCADE)
    bot = models.ForeignKey('Bot', on_delete=models.CASCADE)
    contacted_bot = models.BooleanField(default=False)
    number = models.IntegerField(default=0)
    clue0 = models.BooleanField(default=False)
    clue1 = models.BooleanField(default=False)
    clue2 = models.BooleanField(default=False)
    clue3 = models.BooleanField(default=False)
    clue4 = models.BooleanField(default=False)

    class Meta:
        unique_together = ('earth_user', 'bot', 'number')

    def __str__(self):
        name = str(self.earth_user) + " --  " + str(self.bot) + " -- " + str(self.number)
        return name


class EarthUser(AbstractUser):
    jid = models.CharField(max_length=200)
    trials = models.IntegerField(default=2)

    def __str__(self):
        return self.jid or self.username
