from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin

# Register your models here.

from django.contrib.auth.admin import UserAdmin
from .models import EarthUser, Bot, BotState, MapCoordinate

admin.site.register(EarthUser, UserAdmin)
admin.site.register(BotState)
admin.site.register(Bot)
admin.site.register(MapCoordinate, LeafletGeoAdmin)

# fieldset https://stackoverflow.com/questions/11784191/django-auth-adding-user-fields-displaying-in-admin