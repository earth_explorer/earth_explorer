import sys
import requests
import string
import random
from .models import EarthUser, BotState, Bot
from django.utils.timezone import now

class XmppBackend(object):
    """
    Authenticate with the XMPP 00-70 XEP
    """
    def __init__(self):
        self.transaction_id = None

    def get_transaction_id(self):
        return self.transaction_id
    def set_transaction_id(self, transaction_id):
        self.transaction_id = transaction_id


    def authenticate(self, username=None, password=None, transaction_id = None):
        #  TODO : stocker l'ID dans la BDD
        # Check the token and return a user.
        timeout = 300
        payload = {'jid': username, 'domain': 'earth-explorer.agayon.be', 'method': 'POST', 'timeout': timeout,
                   'transaction_id': transaction_id}
        r = requests.get('https://auth.agayon.be/auth', params=payload)
        #r = requests.get('http://example.org/')
        if r.status_code == 200:
            try:
                user = EarthUser.objects.get(username=username)
                user.connection_date = now
            except EarthUser.DoesNotExist:
                # Create a new user. There's no need to set a password
                user = EarthUser(username=username,)
                user.is_staff = False
                user.is_superuser = False
                user.jid = username
                user.connection_date = now
                user.save()
                # Create the firsts relations:
                # The new user know the Welcome bot and the Welcome bot know the new user.
                for bot in Bot.objects.all():
                    if 'Réceptibot' in bot.name:
                        contact = True
                        bot.contacts_id = user.pk
                        bot.save()
                        # FIXME: Réceptibot is a name for testing purposes ?
                    else:
                        contact = False

                    bs = BotState(earth_user=user, bot=bot, contacted_bot=contact, number=bot.number)
                    bs.save()

            return user, r.status_code
        if r.status_code == 401:
            print("User {} refused to authenticate".format(username), file=sys.stdout)
            return None, r.status_code
        return None, r.status_code

    def get_user(self, user_id):
        try:
            return EarthUser.objects.get(pk=user_id)
        except EarthUser.DoesNotExist:
            return None, None



    def id_generator(self, size=6, chars=string.ascii_letters + string.digits):
        self.transaction_id = ''.join(random.choice(chars) for _ in range(size))
        return self.transaction_id
