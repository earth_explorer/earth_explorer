from django import forms


class AuthForm(forms.Form):
    username = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'placeholder': 'XMPP jid'}))


class ClueForm(forms.Form):
    file = forms.FileField(required=True)


class FinalForm(forms.Form):
    # À titre d'exemple, Baltimore (aux États-Unis) a une latitude de 39,28° nord et une longitude de 76,60° ouest (39° 17′ N, 76° 36′ O
    ns = (('nord', 'Nord'),('sud', 'Sud'))
    ew = (('east', 'Est'), ('west', 'Ouest'))
    latitude_deg = forms.IntegerField(label="°")
    latitude_min = forms.IntegerField(label='"')
    latitude_sec = forms.IntegerField(label="'")
    latitude_ns = forms.ChoiceField(choices=ns)
    longitude_deg = forms.IntegerField(label="°")
    longitude_min = forms.IntegerField(label='"')
    longitude_sec = forms.IntegerField(label="'")
    longitude_ew = forms.ChoiceField(choices=ew)
