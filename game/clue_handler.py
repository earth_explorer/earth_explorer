import time

import io
import pandas as pd
from django.contrib.gis.geos import Point
from os import path

from .models import MapCoordinate

from django.utils.html import format_html
from django.urls import reverse

ACCEPTED_EXTENSIONS = [".csv", ".png", ".xlsx", ".pdf"]
ACCEPTED_EXTENSIONS = [".csv"]


def file_handler(file, earth_user):
    filename, extension = path.splitext(file.name)
    if extension not in ACCEPTED_EXTENSIONS:
        msg = f"Veuillez envoyer un fichier de ce type: {str(ACCEPTED_EXTENSIONS)}"
        return False, msg
    ret, msg, delete_map = clue_handler(file=file, filename=filename, extension=extension, earth_user=earth_user)
    return ret, msg, delete_map


def clue_handler(file, filename, extension, earth_user):
    msg = "La preuve envoyée ne correspond pas à ce que le système attend."
    msg = "Super ! Un indice de plus !"
    ret = False
    if extension == ".csv":
        # We have a CSV file, Probably some coordinates
        ret, msg, delete_map = coordinates_handler(file, filename, earth_user)
    return ret, msg, delete_map


def coordinates_handler(file, filename, earth_user):
    """
    Handle the coordinates
    :param file: file object containing the coordinates
    :param filename:
    :return: None
    """
    csv_reader = file.read().decode('utf-8')
    io_string = io.StringIO(csv_reader)
    epoch = time.time()
    df = pd.read_csv(io_string, )
    for idx, row in df.iterrows():
        try:
            longitude = float(row['col_B'])
            latitude = float(row['col_A'])
            category = row['col_C']
            # name = row['fullname']
            # fullname = row['fullname']
            jid = earth_user.jid
            fullname = f"{jid}_{filename}_{category}"
            if 'name' in row:
                name = row['name']
            else:
                name = fullname
            point = MapCoordinate(name=name, geom=Point(latitude, longitude), earth_user=earth_user, category=category,
                                  latitude=latitude, longitude=longitude, fullname=fullname)
            point.save()
        except KeyError:
            msg = "Erreur de traitement. Votre fichier est mal formé"
            ret = False
            return ret, msg
    msg = format_html("Nouvelles coordonnées ajoutées à votre <a href={}>profil</a>.",
                      reverse('profile'), )
    ret = "La carte a été mise à jour !"
    delete_map = False
    return ret, msg, delete_map
