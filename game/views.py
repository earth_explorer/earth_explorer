from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from .decorators import login_required
from django.contrib.auth import login, logout
from django.shortcuts import render, redirect
from django.core import serializers

from . import xmpp_auth
from .forms import AuthForm, ClueForm, FinalForm
from .models import BotState, EarthUser, MapCoordinate
from .clue_handler import file_handler

LOG_FILE = settings.LOG_FILE


def index(request):
    context = {'session': request.session}
    return render(request, 'game/index.html', context)


def xmpp_authentification(request):
    xb = xmpp_auth.XmppBackend()
    transaction_id = None
    status_msg = ""
    context = {}
    user_jid = request.session.get('user_jid')
    if request.method == 'POST':
        try:
            transaction_id = request.session.get('transaction_id')
        except KeyError:
            request.session['user_logged_in'] = False
            return render(request, 'game/fail.html')
        form = AuthForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            username = form.cleaned_data['username']
            # Log users
            with open(LOG_FILE, 'a') as lf:
                msg = username + '\n'
                lf.write(msg)
            user, status_code = xb.authenticate(username=username, password=None, transaction_id=transaction_id)
            if user is not None:
                login(request, user)

                # Redirect to a success page.
                request.session['user_logged_in'] = True
                request.session['user_jid'] = user.jid
                context['session'] = request.session
                # return render(request, 'game/success.html', context, )
                return redirect('profile')

            if status_code == 401:
                request.session['user_logged_in'] = False
                status_msg = "User {} refused to authenticate.".format(username)
        else:
            request.session['user_logged_in'] = False
            return render(request, 'game/fail.html')
    else:
        request.session['user_logged_in'] = False
        transaction_id = xb.id_generator(6)
        request.session['transaction_id'] = transaction_id
        form = AuthForm()
    context['session'] = request.session
    context['form'] = form
    context['transaction_id'] = transaction_id
    context['status_msg'] = status_msg
    return render(request, 'game/registration/login.html', context)

@login_required
def profile(request):
    context = {'session': request.session, 'major': "profile"}
    me = EarthUser.objects.get(jid=request.session['user_jid'])
    context['user'] = me
    botstates = BotState.objects.filter(earth_user=me).order_by('number')
    map_coordinate = MapCoordinate.objects.filter(earth_user=me)
    map_coordinate_json = serializers.serialize('json', map_coordinate)
    context['botstates'] = botstates
    context['user'] = me
    context['map_coordinate'] = map_coordinate_json
    return render(request, 'game/profile.html', context)

@login_required
def clue(request):
    context = {'session': request.session, 'major': "clue", 'fail': False, 'clue': False, 'uploaded_file': False,
               'msg': "", "delete_map": True}
    try:
        me = EarthUser.objects.get(jid=request.session['user_jid'])
    except KeyError:
        return redirect('login')
    context['user'] = me
    # Sometimes the resquest file is not defined when redirected.
    if request.method == 'POST':
        form = ClueForm(request.POST, request.FILES)
        context['filename'] = "temp_filename"
        context['uploaded_file'] = True
        if 'file' in request.FILES:
            context['filename'] = request.FILES['file'].name
        if form.is_valid():
            ret, msg, delete_map = file_handler(file=request.FILES['file'], earth_user=me)
            context['msg'] = msg
            context['clue'] = ret
            context["uploaded_file"] = True
            context['form'] = None
            context['delete_map'] = delete_map
            context['map_coordinate'] = MapCoordinate.objects.filter(earth_user=me)
            return render(request, 'game/clue.html', context)
    else:
        context["uploaded_file"] = False
        context['map_coordinate'] = MapCoordinate.objects.filter(earth_user=me)
        form = ClueForm()
    context['form'] = form
    return render(request, 'game/clue.html', context)
    # except KeyError:
    #     print('key error')
    #     return redirect('login')


def faq(request):
    context = {'session': request.session}
    return render(request, 'game/faq.html', context)


def help_movim(request):
    context = {'session': request.session}
    return render(request, 'game/help_movim.html', context)

@login_required
def delete(request):
    context = {}
    try:
        me = EarthUser.objects.get(jid=request.session['user_jid'])
    except KeyError:
        return redirect('login')
    context['user'] = me
    me.delete()
    try:
        del request.session['member_id']
    except KeyError:
        pass
    logout(request)
    context['session'] = request.session
    return render(request, 'game/delete_profile.html', context)

@login_required
def clean(request):
    context = {'session': request.session, 'major': "status", "map_coordinate": False}
    try:
        me = EarthUser.objects.get(jid=request.session['user_jid'])
    except KeyError:
        return redirect('login')
    context['user'] = me
    try:
        MapCoordinate.objects.filter(earth_user=me).delete()
    except MapCoordinate.DoesNotExist:
        pass
    return render(request, 'game/profile.html', context)

@login_required
def withdraw(request):
    context = {'session': request.session, 'major': "clue"}
    # prevent the clues to be accessible if not logged
    # prevent the clues to be accessible if the bot has not been joined
    return render(request, 'game/withdraw.html', context)


def steven(request):
    # TODO: https://docs.djangoproject.com/en/2.2/topics/http/sessions/
    # prevent the clues to be accessible if the bot has not been joined ?
    # prevent the clues to be accessible if not logged
    context = {'session': request.session, }
    return render(request, 'game/steven.html', context)


#@login_required
def secret_4d45c10cd25a(request):
    # This is the link in the space image.
    context = {'session': request.session, 'clue': "4d45c10cd25a"}
    # TODO: update clues to add col_B
    return render(request, 'game/withdraw.html', context)

#@login_required
def secret_c265c9f626bs(request):
    # This is the link with the 3rd column: Object White Rabbit
    # TODO: update clues to add col_C
    context = {'session': request.session, 'clue': "lapin_blanc"}
    return render(request, 'game/withdraw.html', context)

@login_required
def bonus(request):
    # TODO: https://docs.djangoproject.com/en/2.2/topics/http/sessions/
    # prevent the clues to be accessible if the bot has not been joined ?
    # prevent the clues to be accessible if not logged
    context = {'session': request.session, 'clue': "bonus"}
    return render(request, 'game/withdraw.html', context)


@login_required
def final(request):
    context = {'session': request.session, }
    try:
        me = EarthUser.objects.get(jid=request.session['user_jid'])
    except KeyError:
        return redirect('login')
    context['user'] = me
    trials = getattr(me, 'trials')
    int2str = {'0': 'zéro','1': 'un', '2': 'deux', '3': 'trois'}
    print(int2str[str(trials)])
    context['trials_str'] = int2str[str(trials)]
    context['trials'] = trials

    if request.method == 'POST':
        form = FinalForm(request.POST, request.FILES)
        context['trial'] = True
        if form.is_valid():
            # TEST IF Coordinate = GOOD
            coordinates = False
            ret = False
            if not coordinates:
                msg = "BAD"
                me.trials = me.trials - 1
                me.save()
            else:
                msg = 'Good'
            context['msg'] = msg
            context['success'] = ret
            context['form'] = None
            context['map_coordinate'] = MapCoordinate.objects.filter(earth_user=me)
            return render(request, 'game/final.html', context)
    else:
        context["uploaded_file"] = False
        context['map_coordinate'] = MapCoordinate.objects.filter(earth_user=me)
        form = FinalForm()
    context['form'] = form
    return render(request, 'game/final.html', context)


@staff_member_required
def status(request):
    context = {'session': request.session, 'major': "status"}
    botstates = BotState.objects.all().order_by('earth_user')
    context['botstates'] = botstates
    return render(request, 'game/status.html', context)
