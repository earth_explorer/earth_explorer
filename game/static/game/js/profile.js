/**
 * Earth explorer
 * Author: Arnaud Joset
  See https://gitlab.com/earth_explorer/earth_explorer

 */

class initProfile {
    constructor(L, mapData, blueIconUrl, violetIconUrl, shadowUrl) {
        this.L = L;
        this.mapData = mapData;
        this.blueIconUrl = blueIconUrl;
        this.violetIconUrl = violetIconUrl;
        this.shadowUrl = shadowUrl;
        this.blueIcon = new L.Icon({
            iconUrl: this.blueIconUrl,
            shadowUrl: this.shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });
        this.violetIcon = new L.Icon({
            iconUrl: this.violetIconUrl,
            shadowUrl: this.shadowUrl,
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            popupAnchor: [1, -34],
            shadowSize: [41, 41]
        });
    }
    getBlueIcon () {
        return this.blueIcon;
    }
    getVioletIcon () {
        return this.violetIcon;
    }

    mainMapInit (map, options) {
        const mapData = window.agayon.mapData;
        const violetIcon = window.agayon.violetIcon;
        const blueIcon = window.agayon.blueIcon;
        mapData.forEach(function (element) {
            var name = element.fields.category;
            var iconImg;
                if (parseInt(name) === 1) {
                    iconImg = violetIcon;
                }
                else {
                    iconImg = blueIcon;
                }
                var latitude = element.fields.latitude;
                var longitude = element.fields.longitude;
                // var geom = element.fields.geom;
                L.marker([longitude, latitude], {icon: iconImg}).addTo(map)
                .bindPopup(name)
                .openPopup();
        });
    }
}

function agayonMap(L, mapData, blueIconUrl, violetIconUrl, shadowUrl){
    return new initProfile (L, mapData, blueIconUrl, violetIconUrl, shadowUrl);
}


