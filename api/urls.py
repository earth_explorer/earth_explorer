from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^bot/$', views.BotList.as_view()),
    url(r'^bot/(?P<pk>[0-9]+)/$', views.BotDetail.as_view()),
    url(r'^earthuser/$', views.EarthUserList.as_view()),
    url(r'^earthuser/(?P<pk>[0-9]+)/$', views.EarthUserDetail.as_view()),
    url(r'^botstate/$', views.BotStateList.as_view()),
    url(r'^botstate/(?P<pk>[0-9]+)/$', views.BotStateDetail.as_view()),
]
