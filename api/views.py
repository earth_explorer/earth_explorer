from django.shortcuts import render

# Create your views here.

from game.models import BotState, EarthUser, Bot
from .serializers import BotSerializer, BotStateSerializer, EarthUserSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions


def index(request):
    context = {'session': request.session}
    return render(request, 'api/index.html', context)


##### BOTS ####
class BotList(APIView):
    """
    List all bots, or create a new bot.
    """

    def get(self, request, format=None):
        bots = Bot.objects.all()
        serializer = BotSerializer(bots, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = BotSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BotDetail(APIView):
    """
    Retrieve, update or delete a bot instance.
    """

    def get_object(self, pk):
        try:
            return Bot.objects.get(pk=pk)
        except Bot.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        bot = self.get_object(pk)
        serializer = BotSerializer(bot)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        bot = self.get_object(pk)
        serializer = BotSerializer(bot, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        bot = self.get_object(pk)
        bot.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


##### BotsStates ####

class BotStateList(APIView):
    """
    List all bots, or create a new bot.
    """

    def get(self, request, format=None):
        botstate = BotState.objects.all()
        serializer = BotStateSerializer(botstate, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = BotStateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BotStateDetail(APIView):
    """
    Retrieve, update or delete a bot instance.
    """

    def get_object(self, pk):
        try:
            return BotState.objects.get(pk=pk)
        except BotState.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        botstate = self.get_object(pk)
        serializer = BotStateSerializer(botstate)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        botstate = self.get_object(pk)
        serializer = BotStateSerializer(botstate, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        botstate = self.get_object(pk)
        botstate.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


##### EarthUser ####

class EarthUserList(APIView):
    """
    List all bots, or create a new bot.
    """

    def get(self, request, format=None):
        earthuser = EarthUser.objects.all()
        serializer = EarthUserSerializer(earthuser, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = EarthUserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EarthUserDetail(APIView):
    """
    Retrieve, update or delete a bot instance.
    """

    def get_object(self, pk):
        try:
            return EarthUser.objects.get(pk=pk)
        except EarthUser.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        earthuser = self.get_object(pk)
        serializer = EarthUserSerializer(earthuser)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        earthuser = self.get_object(pk)
        serializer = EarthUserSerializer(earthuser, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        earthuser = self.get_object(pk)
        earthuser.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
