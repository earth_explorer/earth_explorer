from rest_framework import serializers
from game.models import Bot, BotState, EarthUser


class BotSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = Bot
        fields = ('id', 'number', 'name', 'jid', 'description', 'clue0', 'clue1', 'clue2', 'clue3', 'clue4',
                  'trigger0', 'trigger1', 'trigger2', 'trigger3', 'trigger4')


class BotStateSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = BotState
        fields = ('id', 'earth_user', 'bot', 'contacted_bot', 'clue0', 'clue1', 'clue2', 'clue3', 'clue4',)


class EarthUserSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()

    class Meta:
        model = EarthUser
        fields = ('id', 'username', 'jid',)
